// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SpatialAudio.h"
#include "GameFramework/Actor.h"
#include "PureDataNode.generated.h"

/**
 * 
 */
UCLASS(meta = (ChildCanTick))
class SPATIALAUDIO_API APureDataNode : public AActor
{
	GENERATED_UCLASS_BODY()

protected:
	// BlueprintCallabel: Allows function to be added to a Blueprint
	// BlueprintPure: Means that the function does not depend upon the
	//				  sequence in which it is run ( no white connectors )
	// Category: Put the function in a specific category
	UFUNCTION(BlueprintCallable, Category = SpatialAudio)
		void SendOscInt32(FString header, int32 message);
	UFUNCTION(BlueprintCallable, Category = SpatialAudio)
		void SendOscFloat(FString header, float message);
	/*
	UFUNCTION(BlueprintCallable, Category = SpatialAudio)
	void SendOscInt32(FString header, int32 message);

	UFUNCTION(BlueprintCallable, Category = SpatialAudio)
	void SendOscString(FString header, FString message);
	*/

	/*
	UFUNCTION(BlueprintCallable, Category = SpatialAudio)
	int32 RecordActorLocation(int32 actor);

	UFUNCTION(BlueprintCallable, Category = SpatialAudio)
	FVector PlaybackActorLocation(int32 actor, int32 playback_position);
	*/
	
};
