using System.IO;
using UnrealBuildTool;

public class SpatialAudio : ModuleRules
{
    private string ModulePath
    {
        get { return Path.GetDirectoryName(RulesCompiler.GetModuleFilename(this.GetType().Name)); }
    }

    private string LibrariesPath
    {
        get { return Path.GetFullPath(Path.Combine(ModulePath, "../../Libraries/")); }
    }

    public SpatialAudio(TargetInfo Target)
    {
        PrivateIncludePaths.AddRange(new string[] {
            "SpatialAudio/",
            "SpatialAudio/Public",
            "SpatialAudio/Private",
            "SpatialAudio/ip",
            "SpatialAudio/osc"});

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });

        PublicAdditionalLibraries.Add(Path.Combine(LibrariesPath, "oscpack.lib"));
        //PublicAdditionalLibraries.Add(Path.Combine(LibrariesPath, "winmm.lib"));
        //PublicAdditionalLibraries.Add(Path.Combine(LibrariesPath, "ws2_32.lib"));


    }
}
