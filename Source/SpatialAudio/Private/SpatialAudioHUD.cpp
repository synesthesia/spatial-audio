// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "SpatialAudio.h"
#include "SpatialAudioHUD.h"
#include "Engine/Canvas.h"
#include "TextureResource.h"
#include "CanvasItem.h"
#include "osc/OscOutboundPacketStream.h"
#include "ip/UdpSocket.h"
#include <string>

UdpTransmitSocket *transmitSocket;

ASpatialAudioHUD::ASpatialAudioHUD(const class FPostConstructInitializeProperties& PCIP) : Super(PCIP)
{
	char ADDRESS[] = "127.0.0.1";
	int PORT = 7000;
	
	// Set the crosshair texture
	static ConstructorHelpers::FObjectFinder<UTexture2D> CrosshiarTexObj(TEXT("/Game/Textures/Crosshair"));
	CrosshairTex = CrosshiarTexObj.Object;

	// Instantiate UDP Socket
	if (transmitSocket == NULL) transmitSocket = new UdpTransmitSocket(IpEndpointName(ADDRESS, PORT));
}


void ASpatialAudioHUD::DrawHUD()
{
	Super::DrawHUD();

	// Draw very simple crosshair

	// find center of the Canvas
	const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

	// offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
	const FVector2D CrosshairDrawPosition( (Center.X - (CrosshairTex->GetSurfaceWidth() * 0.5)),
										   (Center.Y - (CrosshairTex->GetSurfaceHeight() * 0.5f)) );

	// draw the crosshair
	FCanvasTileItem TileItem( CrosshairDrawPosition, CrosshairTex->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem( TileItem );
}

void ASpatialAudioHUD::SendOscString(FString header, FString message)
{
	const int OUTPUT_BUFFER_SIZE = 1024;
	char buffer[OUTPUT_BUFFER_SIZE];

	if (header.IsEmpty()) {
		UE_LOG(LogTemp, Warning, TEXT("Error: No header provided"));
	} else {
		UE_LOG(LogTemp, Warning, TEXT("header:%s\nmessage:%s\n"), *header, *message);

		osc::OutboundPacketStream p(buffer, OUTPUT_BUFFER_SIZE);

		p << osc::BeginBundleImmediate
			<< osc::BeginMessage(TCHAR_TO_ANSI(*header))
			<< TCHAR_TO_ANSI(*message) << osc::EndMessage
			<< osc::EndBundle;
		
		transmitSocket->Send(p.Data(), p.Size());
	}
}