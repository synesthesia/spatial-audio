// Fill out your copyright notice in the Description page of Project Settings.

#include "SpatialAudio.h"
#include "PureDataNode.h"
#include "osc/OscOutboundPacketStream.h"
#include "ip/UdpSocket.h"
#include <string>


UdpTransmitSocket *ttransmitSocket;

APureDataNode::APureDataNode(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	char ADDRESS[] = "127.0.0.1";
	int PORT = 7000;
	if (ttransmitSocket == NULL) ttransmitSocket = new UdpTransmitSocket(IpEndpointName(ADDRESS, PORT));
}

void APureDataNode::SendOscInt32(FString header, int32 message)
{
	const int OUTPUT_BUFFER_SIZE = 1024;
	char buffer[OUTPUT_BUFFER_SIZE];

	if (header.IsEmpty()) {
		UE_LOG(LogTemp, Warning, TEXT("Error: Unable to send int32 osc message"));
	} else {
		UE_LOG(LogTemp, Warning, TEXT("header:%s\nmessage:%d"), *header, message);
		
		osc::OutboundPacketStream p(buffer, OUTPUT_BUFFER_SIZE);

		p << osc::BeginBundleImmediate
			<< osc::BeginMessage(TCHAR_TO_ANSI(*header))
			<< message << osc::EndMessage
			<< osc::EndBundle;

		ttransmitSocket->Send(p.Data(), p.Size());
	}
}

void APureDataNode::SendOscFloat(FString header, float message)
{
	const int OUTPUT_BUFFER_SIZE = 1024;
	char buffer[OUTPUT_BUFFER_SIZE];

	if (header.IsEmpty()) {
		UE_LOG(LogTemp, Warning, TEXT("Error: Unable to send int32 osc message"));
	} else {
		UE_LOG(LogTemp, Warning, TEXT("header:%s\nmessage:%d"), *header, message);
		
		osc::OutboundPacketStream p(buffer, OUTPUT_BUFFER_SIZE);

		p << osc::BeginBundleImmediate
			<< osc::BeginMessage(TCHAR_TO_ANSI(*header))
			<< message << osc::EndMessage
			<< osc::EndBundle;

		ttransmitSocket->Send(p.Data(), p.Size());
	}
}

/*
void AMyActor::SendOscInt32(FString header, int32 message)
{
char buffer[OUTPUT_BUFFER_SIZE];
//char HEADER[] = "/test2";

if(header.IsEmpty()) {
UE_LOG(LogTemp, Warning, TEXT("Error: Unable to send int32 osc message"));
} else {
UE_LOG(LogTemp, Warning, TEXT("header:%s\nmessage:%d"), *header, message);

osc::OutboundPacketStream p(buffer, OUTPUT_BUFFER_SIZE);

p << osc::BeginBundleImmediate
<< osc::BeginMessage(TCHAR_TO_ANSI(*header))
<< message << osc::EndMessage
<< osc::EndBundle;

transmitSocket->Send(p.Data(), p.Size());
}
}

void AMyActor::SendOscString(FString header, FString message)
{
char buffer[OUTPUT_BUFFER_SIZE];
//char HEADER[] = "/test2";

if (header.IsEmpty() || message.IsEmpty()) {
UE_LOG(LogTemp, Warning, TEXT("Error: Unable to send string osc message\n%s"), *message);
} else {
UE_LOG(LogTemp, Warning, TEXT("header:%s\nmessage:%s"), *header, *message);

osc::OutboundPacketStream p(buffer, OUTPUT_BUFFER_SIZE);

p << osc::BeginBundleImmediate
<< osc::BeginMessage(TCHAR_TO_ANSI(*header))
<< TCHAR_TO_ANSI(*message) << osc::EndMessage
<< osc::EndBundle;

transmitSocket->Send(p.Data(), p.Size());
}
}
*/