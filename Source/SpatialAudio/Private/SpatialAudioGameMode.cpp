// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "SpatialAudio.h"
#include "SpatialAudioGameMode.h"
#include "SpatialAudioHUD.h"
#include "SpatialAudioCharacter.h"

ASpatialAudioGameMode::ASpatialAudioGameMode(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Blueprints/MyCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ASpatialAudioHUD::StaticClass();
}
