% input_image = 'C:/Users/tristan/Documents/Unreal Projects/SpatialAudio/Content/Textures/joey.jpg';
function CreateCubeTexture( input_image_path )
    %% Create a cube texture from an input image path
    % input_image_path - path to a square image
    
    output_image_path = strcat(input_image_path(1:size(input_image_path,2)-4), '_Texture.jpg');
    
    % Read in image
    in_image = imread(input_image_path);
    out_image = [];
    % Get size of input and compute output size
    in_size = size(in_image);
    out_size = in_size;
    out_size(1:2) = out_size(1:2) * 3;
    % Create output image
    out_image = uint8(zeros(out_size));
    out_image(:,:,:) = 255;

    % Panel image as bellow
    % 1
    % 4 5 
    % 7 8 9
    map = [1,0,0;1,1,0;1,1,1];
    x_offset = 0;
    y_offset = 0;
    for x = 1:3
        for y = 1:3
            if map(x,y) == 1
                out_image(1+x_offset:in_size(1)+x_offset, 1+y_offset:in_size(2)+y_offset, 1:in_size(3)) = in_image(:,:,:);
            end
            y_offset = y_offset + 256;
        end
        y_offset = 0;
        x_offset = x_offset + 256;
    end
    
    imwrite(out_image, output_image_path, 'jpg');
end

