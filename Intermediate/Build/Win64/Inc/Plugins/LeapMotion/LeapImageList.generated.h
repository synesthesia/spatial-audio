// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	C++ class header boilerplate exported from UnrealHeaderTool.
	This is automatically generated by the tools.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectBase.h"

#ifdef LEAPMOTION_LeapImageList_generated_h
#error "LeapImageList.generated.h already included, missing '#pragma once' in LeapImageList.h"
#endif
#define LEAPMOTION_LeapImageList_generated_h

#define ULeapImageList_EVENTPARMS
#define ULeapImageList_RPC_WRAPPERS \
	DECLARE_FUNCTION(execgetIndex) \
	{ \
		P_GET_PROPERTY(UIntProperty,index); \
		P_FINISH; \
		*(class ULeapImage**)Result=this->getIndex(index); \
	}


#define ULeapImageList_CALLBACK_WRAPPERS
#define ULeapImageList_INCLASS \
	private: \
	static void StaticRegisterNativesULeapImageList(); \
	friend LEAPMOTION_API class UClass* Z_Construct_UClass_ULeapImageList(); \
	public: \
	DECLARE_CLASS(ULeapImageList, UObject, COMPILED_IN_FLAGS(0), 0, LeapMotion, NO_API) \
	/** Standard constructor, called after all reflected properties have been initialized */    NO_API ULeapImageList(const class FPostConstructInitializeProperties& PCIP); \
	DECLARE_SERIALIZER(ULeapImageList) \
	/** Indicates whether the class is compiled into the engine */    enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#undef UCLASS_CURRENT_FILE_NAME
#define UCLASS_CURRENT_FILE_NAME ULeapImageList


#undef UCLASS
#undef UINTERFACE
#if UE_BUILD_DOCS
#define UCLASS(...)
#else
#define UCLASS(...) \
ULeapImageList_EVENTPARMS
#endif


#undef GENERATED_UCLASS_BODY
#undef GENERATED_IINTERFACE_BODY
#define GENERATED_UCLASS_BODY() \
public: \
	ULeapImageList_RPC_WRAPPERS \
	ULeapImageList_CALLBACK_WRAPPERS \
	ULeapImageList_INCLASS \
public:


